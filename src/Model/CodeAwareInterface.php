<?php
/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 07/06/2022
 * Time: 14:01.
 */

namespace HB\ResourceBundle\Model;

interface CodeAwareInterface
{
    public function getCode(): ?string;

    public function setCode(?string $code): void;
}
