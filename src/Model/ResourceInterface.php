<?php
/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 07/06/2022
 * Time: 14:08.
 */

namespace HB\ResourceBundle\Model;

interface ResourceInterface
{
    public function getId(): ?int;

    public function setId(int $id): self;
}
