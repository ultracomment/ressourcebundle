<?php
/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 07/06/2022
 * Time: 14:09.
 */

namespace HB\ResourceBundle\Model;

interface SoftDeletesInterface
{
    public function getDeletedAt(): ?\DateTimeInterface;

    public function setDeletedAt(\DateTimeInterface $deletedAt): self;
}
