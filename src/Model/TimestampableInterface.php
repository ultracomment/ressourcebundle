<?php
/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 07/06/2022
 * Time: 14:10.
 */

namespace HB\ResourceBundle\Model;

interface TimestampableInterface
{
    public function getCreatedAt(): ?\DateTimeInterface;

    /** @psalm-suppress MissingReturnType
     */
    public function setCreatedAt(?\DateTimeInterface $createdAt): self;

    public function getUpdatedAt(): ?\DateTimeInterface;

    /** @psalm-suppress MissingReturnType
     */
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self;
}
