<?php
/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 07/06/2022
 * Time: 14:12.
 */

namespace HB\ResourceBundle\Model;

use Carbon\Carbon;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait TimestampableTrait
{
    #[ORM\Column(name: 'created_at', type: Types::DATE_MUTABLE, nullable: true)]
    protected ?\DateTimeInterface $createdAt;
    #[ORM\Column(name: 'updated_at', type: Types::DATE_MUTABLE, nullable: true)]
    protected ?\DateTimeInterface $updatedAt;

    /**
     * Gets triggered only on insert.
     */
    #[ORM\PrePersist()]
    public function onPrePersist(): void
    {
        $this->createdAt = Carbon::now();
        $this->updatedAt = Carbon::now();
    }

    /**
     * Gets triggered every time on update.
     */
    #[ORM\PreUpdate()]
    public function onPreUpdate(): void
    {
        $this->updatedAt = Carbon::now();
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }
}
