<?php
/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 07/06/2022
 * Time: 14:13.
 */

namespace HB\ResourceBundle\Model;

interface ToggleableInterface
{
    /**
     * Missing scalar typehint because it conflicts with AdvancedUserInterface.
     */
    public function isEnabled(): bool;

    /**
     * @param bool $enabled
     */
    public function setEnabled(?bool $enabled): void;

    public function enable(): void;

    public function disable(): void;
}
