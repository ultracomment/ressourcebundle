<?php

namespace HB\ResourceBundle\Tests\SoftDeleteable\Fixture;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use HB\ResourceBundle\Model\SoftDeletesTrait;

/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 08/06/2022
 * Time: 11:03.
 */
class UsingTrait
{
    use SoftDeletesTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    private int $id;
}
