<?php

namespace HB\ResourceBundle\Tests\SoftDeleteable;

use HB\ResourceBundle\Tests\SoftDeleteable\Fixture\UsingTrait;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 08/06/2022
 * Time: 11:13.
 */
class SoftDeletableEntityTraitTest extends TestCase
{
    public function testGetSetDeletedAt(): void
    {
        $time = new \DateTime();
        $entity = new UsingTrait();
        $this->assertNull($entity->getDeletedAt(), 'deletedAt defaults to null');
        $this->assertFalse($entity->isDeleted(), 'isDeleted defaults to false');
        $this->assertSame($entity, $entity->setDeletedAt($time), 'Setter has a fluid interface');
        $this->assertSame($time, $entity->getDeletedAt(), 'Getter returns a DateTime Object');
        $this->assertTrue($entity->isDeleted(), 'Is deleted is true when deleteAt is not equal to null');
        $this->assertSame($entity, $entity->setDeletedAt(), 'Setting deletedAt to null undeletes object');
        $this->assertFalse($entity->isDeleted(), 'isDeleted should now return false');
    }
}
