<?php

namespace HB\ResourceBundle\Tests\Timestampable\Fixture;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use HB\ResourceBundle\Model\TimestampableTrait;

/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 07/06/2022
 * Time: 15:01.
 */
#[
    ORM\Entity(),
    ORM\HasLifecycleCallbacks(),
]
final class UsingTrait
{
    use TimestampableTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    private $id;
}
