<?php

namespace HB\ResourceBundle\Tests\Timestampable;
use Doctrine\Common\EventManager;
use HB\ResourceBundle\Tests\Timestampable\Fixture\UsingTrait;
use HB\ResourceBundle\Tests\tools\DatabaseTestCase;

/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 07/06/2022
 * Time: 14:53.
 */
class TimestampableTest extends DatabaseTestCase
{
    public const TARGET = UsingTrait::class;

    protected function setUp(): void
    {
        parent::setUp();
        $evm = new EventManager();
        $this->getDefaultMockSqliteEntityManager($evm);
    }

    public function testShouldTimestampUsingTrait(): void
    {
        $object = new UsingTrait();
        $this->em->persist($object);
        $this->em->flush();
        $this->assertNotNull($object->getCreatedAt());
        $this->assertNotNull($object->getUpdatedAt());
    }

    public function testTraitMethodthShouldReturnObject(): void
    {
        $object = new UsingTrait();
        static::assertInstanceOf(UsingTrait::class, $object->setCreatedAt(new \DateTime()));
        static::assertInstanceOf(UsingTrait::class, $object->setUpdatedAt(new \DateTime()));
    }

    protected function getUsedEntityFixtures(): array
    {
        return [
            self::TARGET,
        ];
    }
}
