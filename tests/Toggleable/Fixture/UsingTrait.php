<?php

namespace HB\ResourceBundle\Tests\Toggleable\Fixture;


use HB\ResourceBundle\Model\ToggleableTrait;

/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 08/06/2022
 * Time: 15:29.
 */
class UsingTrait
{
    use ToggleableTrait;
}
