<?php

namespace HB\ResourceBundle\Tests\tools;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Driver;
use Doctrine\DBAL\Logging\Middleware;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Mapping\Driver\AttributeDriver;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\Persistence\Mapping\Driver\MappingDriver;
use Gedmo\Tool\Logging\DBAL\QueryAnalyzer;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 07/06/2022
 * Time: 15:59.
 */
abstract class DatabaseTestCase extends TestCase
{
    public const TESTS_TEMP_DIR = '/doctrine-extension-tests';

    /**
     * @var EntityManager|null
     */
    protected $em;

    /**
     * @var QueryAnalyzer
     */
    protected $queryAnalyzer;

    /**
     * @var MockObject&LoggerInterface
     */
    protected $queryLogger;

    protected function setUp(): void
    {
        $this->queryLogger = $this->createMock(LoggerInterface::class);
    }

    /**
     * EntityManager mock object together with
     * annotation mapping driver and pdo_sqlite
     * database in memory.
     */
    protected function getDefaultMockSqliteEntityManager(EventManager $evm = null, Configuration $config = null): EntityManager
    {
        $conn = [
            'driver' => 'pdo_sqlite',
            'memory' => true,
        ];

        $config = null === $config ? $this->getDefaultConfiguration() : $config;
        $em = EntityManager::create($conn, $config, $evm ?: $this->getEventManager());

        $schema = array_map(static function ($class) use ($em) {
            return $em->getClassMetadata($class);
        }, $this->getUsedEntityFixtures());

        $schemaTool = new SchemaTool($em);
        $schemaTool->dropSchema([]);
        $schemaTool->createSchema($schema);

        return $this->em = $em;
    }

    /**
     * Creates default mapping driver.
     */
    protected function getMetadataDriverImplementation(): MappingDriver
    {
        if (PHP_VERSION_ID >= 80000) {
            return new AttributeDriver([]);
        }

        return new AnnotationDriver($_ENV['annotation_reader']);
    }

    /**
     * Get a list of used fixture classes.
     *
     * @phpstan-return list<class-string>
     */
    abstract protected function getUsedEntityFixtures(): array;

    protected function getDefaultConfiguration(): Configuration
    {
        $config = new Configuration();
        $config->setProxyDir(DatabaseTestCase::TESTS_TEMP_DIR);
        $config->setProxyNamespace('Proxy');
        $config->setMetadataDriverImpl($this->getMetadataDriverImplementation());
        if (class_exists(Middleware::class)) {
            $config->setMiddlewares([
                new Middleware($this->queryLogger),
            ]);
        }

        return $config;
    }

    /**
     * Build event manager.
     */
    private function getEventManager(): EventManager
    {
        $evm = new EventManager();

        return $evm;
    }
}
